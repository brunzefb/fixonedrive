﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.CommandLineUtils;

namespace FixOneDrive
{
    public class ListWithSize
    {
        public List<string> List {get;set;}
        public long Size {get;set;}

        public ListWithSize()
        {
            List = new List<string>();
        }
    }

    public class Program
    {
        public static CommandOption FixFolderNameOption;
        public static CommandOption FixFolderLengthOption;
        public static CommandOption FixFileBadCharsOption;
        public static CommandOption ViewOnlyOption;
        public static CommandOption RemoveSpacesOption;
        public static CommandOption DuplicatesOption;

        public static CommandOption OptimizedOption;
        public static CommandArgument FolderNameArgument;

        public static List<FileInfo> CameraRoll = new List<FileInfo>();
        public static List<FileInfo> GoogleDrivePhotosList = new List<FileInfo>();

        public static Dictionary<string, ListWithSize> Dict = new Dictionary<string, ListWithSize>();
        public static string BeyondExe = @"C:\Program Files\Beyond Compare 4\BCompare.exe";
        public static string BeyondFlags = " /fv=\"Folder Compare\" /solo";

        public static void Main(string[] args)
        {
            var app = new CommandLineApplication
            {
                Name = "FixOneDrive",
                Description = "Fix OneDrive files/folder names",
            };

            app.HelpOption("-?|-h|--help");

            app.Command("folder",
                (command) =>
                {
                    FixFolderNameOption = command.Option("-f|--fixfolder",
                        "Automatically rename bad bad chars in directories with _",
                        CommandOptionType.NoValue);
                    FixFolderLengthOption = command.Option("-s|--shortenfile",
                        "Allow renaming directories longer than 32 chars",
                        CommandOptionType.NoValue);
                    FixFileBadCharsOption = command.Option("-e|--fixfile",
                        "Auto rename files with bad chars",
                        CommandOptionType.NoValue);
                    ViewOnlyOption = command.Option("-v|--viewonly",
                        "Show, but don't do the mods",
                        CommandOptionType.NoValue);
                    RemoveSpacesOption = command.Option("-r|--removespaces",
                        "Also replace spaces by _",
                        CommandOptionType.NoValue);
                    DuplicatesOption = command.Option("-d|--dup",
                        "Find and report duplicate files and generate Duplicates.txt in the bin folder",
                        CommandOptionType.NoValue);
                    OptimizedOption = command.Option("-z|--optimized",
                        "Find and report duplicate files and generate Duplicates.txt in the bin folder",
                        CommandOptionType.NoValue);

                    FolderNameArgument = command.Argument("folder", "Folder", false);
                    command.Description = "Specify folder";
                    command.OnExecute(() =>
                    {
                        if (!Directory.Exists(FolderNameArgument.Value))
                        {
                            Console.WriteLine("Bad directory");
                            return 1;
                        }

                        if (OptimizedOption.HasValue())
                        {
                            const string od = @"d:\OneDrive\Pictures\Camera_Roll";
                            var di = new DirectoryInfo(od);
                            CameraRoll = di.GetFiles("*.jpg").ToList();
                        }

                        Console.WriteLine("FixOneDrive starting...");
                        ProcessDirectory(FolderNameArgument.Value);
                        
                        if(DuplicatesOption.HasValue())
                            ReportDuplicates();
                        
                        
                        if (OptimizedOption.HasValue())
                        {
                            foreach (var fiOneDriveCameraRoll in CameraRoll)
                            {
                                if (GoogleDrivePhotosList.Any(x=>x.Name == fiOneDriveCameraRoll.Name))
                                {
                                    var name = GoogleDrivePhotosList.FirstOrDefault(x=>x.Name == fiOneDriveCameraRoll.Name).FullName;
                                    Console.WriteLine($"{fiOneDriveCameraRoll.FullName} found in {name}, so deleting it");
                                    File.Delete(fiOneDriveCameraRoll.FullName);
                                }
                                else
                                {
                                    Console.WriteLine($"{fiOneDriveCameraRoll.FullName} not found, moving to c:\\Users\\mbrun\\Google Drive\\Google Photos\\FromOneDriveCameraRoll");
                                    File.Copy(fiOneDriveCameraRoll.FullName, @"c:\Users\mbrun\Google Drive\Google Photos\FromOneDriveCameraRoll\" + fiOneDriveCameraRoll.Name);
                                    try
                                    {
                                        File.Delete(fiOneDriveCameraRoll.FullName);
                                    }
                                    catch
                                    {
                                        Console.WriteLine("error deleting " + fiOneDriveCameraRoll.FullName);
                                    }
                                }
                            }
                        }
                        Console.WriteLine("Done.");
                        return 0;
                    });
                });
    
            app.Execute(args);
        }

        public static void HandleOptimized(string folder)
        {
            var di = new DirectoryInfo(folder);
            var fileEntries = di.GetFiles("*.jpg");
            GoogleDrivePhotosList.AddRange(fileEntries);
        }

        public static void ProcessDirectory(string targetDirectory)
        {
            var di = new DirectoryInfo(targetDirectory);
            if (!di.Exists)
            {
                Console.WriteLine($"Directory {targetDirectory} does not exist");
                return;
            }

            // depth first recursion
            try
            {
                var subdirectoryEntries = Directory.GetDirectories(di.FullName);
                foreach (var subdirectory in subdirectoryEntries)
                    ProcessDirectory(subdirectory);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception: {e.Message} for folder {di.FullName}");
            }

            if (OptimizedOption.HasValue())
            {
                HandleOptimized(di.FullName);
                return;
            }

            Console.WriteLine($"Processing Folder: {targetDirectory}");
            if (FixFileBadCharsOption.HasValue() || DuplicatesOption.HasValue())
            {
                if (FixFileBadCharsOption.HasValue() )
                    Console.WriteLine("Fixing Files");
                var fileEntries = di.GetFiles("*.*");
                foreach (var fileInfo in fileEntries)
                {
                    if (FixFileBadCharsOption.HasValue() )
                        ProcessFile(fileInfo);
                    if (DuplicatesOption.HasValue())
                        ProcessDuplicateFile(fileInfo);
                }
            }

            if (FixFolderLengthOption.HasValue())
                di = ShortenLongDirectories(di);

            if (FixFolderNameOption.HasValue())
                di = RemoveNonAsciiCharsFromDirectory(di);
            
            
        }

        private static DirectoryInfo CheckForDuplicateFolder(DirectoryInfo di)
        {
            var subdirectoryEntries = Directory.GetDirectories(di.FullName);
            var fileEntries = di.GetFiles("*.*");
            if (subdirectoryEntries.Length + fileEntries.Length == 0)
                return di;                        
            if (!Dict.ContainsKey(di.Name))
            {
                var listSize = new ListWithSize();
                listSize.List.Add(di.FullName);
                
                listSize.Size = 0;//subdirectoryEntries.Length + fileEntries.Length;
                Dict.Add(di.Name, listSize);
            }
            else
            {
                var obj = Dict[di.Name];
                if (!obj.List.Contains(di.FullName))
                    obj.List.Add(di.FullName);
                
            }
            return di;
        }

        private static DirectoryInfo RemoveNonAsciiCharsFromDirectory(DirectoryInfo di)
        {
            var lastDirectoryName = di.Name;
            var bytes = Encoding.ASCII.GetBytes(lastDirectoryName);
            var asciiName = Encoding.ASCII.GetString(bytes);
            asciiName = asciiName.Replace("?", "_");
            if (asciiName == lastDirectoryName && !RemoveSpacesOption.HasValue())
                return di;
            if (di.Parent == null)
                return di;

            if (RemoveSpacesOption.HasValue())
            {
                asciiName = asciiName.Replace(' ', '_');
                asciiName = asciiName.Replace("&", "_and_");
                asciiName = asciiName.Replace("'", "");
                asciiName = asciiName.Replace(",", "_");
                asciiName = asciiName.Replace("+", "_");
                asciiName = asciiName.Replace(".", "_");
                asciiName = asciiName.Replace("@", "_at_");

            }

            var newName = Path.Combine(di.Parent.FullName, asciiName);
            try
            {
                if (!ViewOnlyOption.HasValue() && di.FullName != newName)
                {
                    Directory.Move(di.FullName, newName);
                    Console.Write($"Renamed '{di.FullName}' to '{newName}'");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + $"Renaming {di.FullName} to {newName} failed");
                return di;
            }

           
            di = new DirectoryInfo(newName);

            return di;
        }

        public static DirectoryInfo ShortenLongDirectories(DirectoryInfo di)
        {
            var lastDirectoryName = di.Name;
            if (lastDirectoryName.Length <= 32)
                return di;
            
            Console.WriteLine($"{lastDirectoryName}' is longer than 32 chars, [r]=rename, [k]=keep");
            var response = Console.ReadLine();
            if (response != null && response.ToLower().StartsWith("r"))
            {
                Console.Write("Enter new shorter directory name: ");
                var newName = Console.ReadLine();
                if (di.Parent == null) return di;
                var newFullName = Path.Combine(di.Parent.FullName, newName);
                try
                {
                    if (!ViewOnlyOption.HasValue())
                        Directory.Move(di.FullName, newFullName);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message + $"Renaming \n{di.FullName} to \n{newFullName} failed");
                    return di;
                }
                    
                Console.Write($"Renamed \n'{di.FullName}' to \n'{newFullName}'\n");
                di = new DirectoryInfo(newFullName);
            }
            return di;
        }

        public static void ProcessFile(FileInfo path)
        {
            if (!path.Exists)
                return;

            var fileInfo = path;
            var bytes = Encoding.ASCII.GetBytes(fileInfo.Name);
            var asciiName = Encoding.ASCII.GetString(bytes);
            asciiName = asciiName.Replace("?", "_");
            if (asciiName == fileInfo.Name &&!RemoveSpacesOption.HasValue())
                return;
            if (RemoveSpacesOption.HasValue())
            {
                asciiName = asciiName.Replace(' ', '_');
                asciiName = asciiName.Replace("&", "_and_");
                asciiName = asciiName.Replace("'", "");
                asciiName = asciiName.Replace(",", "_");
                asciiName = asciiName.Replace("+", "_");
                asciiName = asciiName.Replace("@", "_at_");

            }
            var newName = Path.Combine(fileInfo.DirectoryName, asciiName);
            Console.WriteLine($"Renaming filename: {path} to {newName}");
               
            try
            {
                if (!ViewOnlyOption.HasValue() && path.FullName !=newName)
                    File.Move(path.FullName, newName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + $"Renaming File: {path} to {newName} failed");
            }
        }

        private static void ProcessDuplicateFile(FileInfo fi)
        {
            if (!Dict.ContainsKey(fi.Name))
            {
                var listSize = new ListWithSize();
                listSize.List.Add(Path.GetDirectoryName(fi.FullName));
                listSize.Size = fi.Length;
                Dict.Add(fi.Name, listSize);
            }
            else
            {
                var obj = Dict[fi.Name];
                if (obj.Size == fi.Length)
                {
                    obj.List.Add(Path.GetDirectoryName(fi.FullName));
                }
            }
        }

        private static void ReportDuplicates()
        {
            var sb = new StringBuilder();
            foreach (var v in Dict.Keys)
            {
                if (Dict[v].List.Count > 1)
                {
                    sb.AppendLine($"File: {v}, Length: {Dict[v].Size}");
                    foreach(var d in Dict[v].List)
                    {
                        sb.AppendLine($"    {d}");
                    }
                    sb.AppendLine(" ");
                }
            }
            File.WriteAllText("Duplicates.txt", sb.ToString());
        }

        private static void HandleDuplicateFolders()
        {
            int count = 0;
            int current = 0;
            int countall = 0;

            foreach (var v in Dict.Keys)
            {
                if (Dict[v].List.Count == 2)
                {
                    ++count;
                    ++countall;
                }
                else
                    ++countall;
            }

            Console.WriteLine($"There are {countall} total duplicate folders with {count} having exactly 2 duplicate folders");
            foreach (var v in Dict.Keys)
            {
                if (Dict[v].List.Count != 2)
                    continue;
                ++current;

                Console.WriteLine($"Duplicate folder '{v} found!  ({current} of {count})");
                for(var i=0; i<Dict[v].List.Count; i++)
                {
                    Console.WriteLine($"{i+1}: '{Dict[v].List[i]}'");
                }
                Console.WriteLine($"Comparing {Dict[v].List[0]} and {Dict[v].List[1]}");
                System.Diagnostics.Process.Start(BeyondExe, Dict[v].List[0] + " " + Dict[v].List[1] + BeyondFlags );
                Console.WriteLine("Launching beyond compare...");
                System.Threading.Thread.Sleep(2000);
                Console.WriteLine("Enter what Folder to KEEP (the other will be deleted!) '1' or '2' , 's' to skip, 'a' to abort");
                while (true)
                {
                    ConsoleKeyInfo cki = Console.ReadKey(true);
                    if (cki.Key == ConsoleKey.D1)
                    {
                        RecursiveDelete(new DirectoryInfo(Dict[v].List[1]));      
                        break;
                    }
                    else if (cki.Key == ConsoleKey.D2)
                    {
                        RecursiveDelete(new DirectoryInfo(Dict[v].List[0]));      
                        break;
                    }
                    else if (cki.Key == ConsoleKey.S)
                    {
                        break;
                    }
                    else if (cki.Key == ConsoleKey.A)
                    {
                        Console.WriteLine("Quitting!");
                        Environment.Exit(1);
                    }
                    else 
                        Console.WriteLine("Wrong key! - enter '1', '2', 's' or 'a'");
                }
            }
            
        }

        public static void RecursiveDelete(DirectoryInfo baseDir)
        {
            if (!baseDir.Exists)
                return;

            foreach (var dir in baseDir.EnumerateDirectories())
            {
                RecursiveDelete(dir);
            }
            baseDir.Delete(true);
        }
    }
}
